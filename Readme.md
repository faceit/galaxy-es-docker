Dockerfile for Galaxy-ES.


* On Docker Hub at [faceitproject/galaxy-es](https://registry.hub.docker.com/u/faceitproject/galaxy-es/). Docker Hub rebuilds the image every time this repository is pushed to.
* Currently, this Dockerfile configures Galaxy-ES to:
	* Use a SQLite database.
	* Give admin privileges to a user with the email address galaxy-es-user@faceit-portal.org. (This is not a real email address, but create a user with that address and it will have admin privileges)

Further instructions on how to install Galaxy-ES with Docker are available [here](http://www.faceit-portal.org/for-developers/installing-a-galaxy-es-virtual-machine).